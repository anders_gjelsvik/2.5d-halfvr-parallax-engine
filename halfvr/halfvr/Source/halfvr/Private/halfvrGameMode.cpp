// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "halfvrPrivate.h"
#include "halfvrGameMode.h"
#include "halfvrCharacter.h"
#include "HalfvrPlayerController.h"

AhalfvrGameMode::AhalfvrGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

	// set default controller class
	PlayerControllerClass = AHalfvrPlayerController::StaticClass();

	// set default pawn class to our character
	DefaultPawnClass = AhalfvrCharacter::StaticClass();	
	

}
