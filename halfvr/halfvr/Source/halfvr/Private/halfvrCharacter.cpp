// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#include "halfvrPrivate.h"
#include "halfvrCharacter.h"
#include "HalfvrPlayerController.h"
#include "PaperFlipbookComponent.h"
#include "EngineUtils.h"
#include <fstream>

//////////////////////////////////////////////////////////////////////////
// AhalfvrCharacter

AhalfvrCharacter::AhalfvrCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer) {

	// Setup the assets
	struct FConstructorStatics {
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> RunningAnimationAsset;
		ConstructorHelpers::FObjectFinderOptional<UPaperFlipbook> IdleAnimationAsset;
		FConstructorStatics()
			: RunningAnimationAsset(TEXT("/Game/Flipbooks/Green_Right"))
			, IdleAnimationAsset(TEXT("/Game/Flipbooks/Green_Idle"))
		{
		}
	};
	static FConstructorStatics ConstructorStatics;

	// Transform object for translating camera in wrap-around view
	trans = new FTransform();

	RunningAnimation = ConstructorStatics.RunningAnimationAsset.Get();
	IdleAnimation = ConstructorStatics.IdleAnimationAsset.Get();
	GetSprite()->SetFlipbook(IdleAnimation);

	// We don't want the character to rotate from controller
	// Controller controls only camera
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw	= false; 
	bUseControllerRotationRoll	= false;

	// Set the size of our collision capsule.
	GetCapsuleComponent()->SetCapsuleHalfHeight(96.0f);
	GetCapsuleComponent()->SetCapsuleRadius(40.0f);

	// Create a camera boom attached to the root (capsule)
	CameraBoom = ObjectInitializer.CreateDefaultSubobject<USpringArmComponent>(this, TEXT("CameraBoom"));
	CameraBoom->AttachTo(RootComponent);	

	// Set some camera info with code
	CameraBoom->TargetArmLength = 1500.0f;							// Distance between camera and character
	CameraBoom->RelativeRotation = FRotator(0.0f, -90.0f, 0.0f);	// Rotate the camera to fit set a 2D view
	
	// Disable collision on cameraboom to prevent camera from skipping forward when the player is technically behind an object
	CameraBoom->bDoCollisionTest = false;

	// Create an camera and attach it to the boom
	SideViewCameraComponent = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("SideViewCamera"));
	SideViewCameraComponent->ProjectionMode = ECameraProjectionMode::Perspective;
	SideViewCameraComponent->AttachTo(CameraBoom, USpringArmComponent::SocketName);
	SideViewCameraComponent->GetComponentRotation();
	// Prevent all automatic rotation behavior on the camera, character, and camera component
	CameraBoom->bAbsoluteRotation = true;						// Camera will use absolute rotation
	SideViewCameraComponent->bUsePawnControlRotation = false;	// If not debugging with oculus this has to be false for normal view atm
	GetCharacterMovement()->bOrientRotationToMovement = false;
	//SideViewCameraComponent->GetComponentRotation();

	// Configure character movement
	GetCharacterMovement()->GravityScale = 2.0f;
	GetCharacterMovement()->AirControl = 0.80f;
	GetCharacterMovement()->JumpZVelocity = 1200.f;
	GetCharacterMovement()->GroundFriction = 3.0f;
	GetCharacterMovement()->MaxWalkSpeed = 600.0f;
	GetCharacterMovement()->MaxFlySpeed = 600.0f;

	// Lock character motion onto the XZ plane, so the character can't move in or out of the screen
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->SetPlaneConstraintNormal(FVector(0.0f, -1.0f, 0.0f));

	// Behave like a traditional 2D platformer character, with a flat bottom instead of a curved capsule bottom
	// Note: This can cause a little floating when going up inclines; you can choose the tradeoff between better
	// behavior on the edge of a ledge versus inclines by setting this to true or false
	GetCharacterMovement()->bUseFlatBaseForFloorChecks = true;

	// Enable replication on the Sprite component so animations show up when networked
	GetSprite()->SetIsReplicated(true);
	bReplicates = true;

	facingRight = true;
	maxRotation = 180.0f;
	ORMultiplier = 20.0f;
	elapsedTime = 0.0f;

	// Create the parallax scrolling effect
	parallaxEffect = new HalfVRParallaxEffect(1, 1, 1, 0, -200, 10, 3, 2.5, false);

	// Set tick to update
	PrimaryActorTick.bCanEverTick = true;

	// Create protected instance of the CameraBoom
	boom = CameraBoom;
}

//////////////////////////////////////////////////////////////////////////
// Game begin ( Will be called at startup
void AhalfvrCharacter::BeginPlay() {
	Super::BeginPlay();

	// Rotate the character to face desired direction at the start
	// At the moment this is momentary solution because of camera
	hvr_rotateRight();
}

//////////////////////////////////////////////////////////////////////////
// Animation
void AhalfvrCharacter::UpdateAnimation(float value) {
	const FVector PlayerVelocity = GetVelocity();
	const float PlayerSpeed = PlayerVelocity.Size();

	// Are we moving or standing still?
	UPaperFlipbook* DesiredAnimation = (PlayerSpeed > 0.0f) ? RunningAnimation : IdleAnimation;

	GetSprite()->SetFlipbook(DesiredAnimation);
}

//////////////////////////////////////////////////////////////////////////
// Input

void AhalfvrCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent) {
	// Note: the 'Jump' action and the 'MoveRight' axis are bound to actual keys/buttons/sticks in DefaultInput.ini (editable from Project Settings..Input)
	InputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	InputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	InputComponent->BindAxis("lookRight", this, &AhalfvrCharacter::lookRight);
	InputComponent->BindAxis("lookUp", this, &AhalfvrCharacter::lookUp);
	InputComponent->BindAxis("MoveRight", this, &AhalfvrCharacter::MoveRight);

	InputComponent->BindTouch(IE_Pressed, this, &AhalfvrCharacter::TouchStarted);
	InputComponent->BindTouch(IE_Released, this, &AhalfvrCharacter::TouchStopped);
}

void AhalfvrCharacter::MoveRight(float Value) {
	// Update animation to match the motion
	UpdateAnimation(Value);
	value = Value;

	// Set the rotation so that the character faces his direction of travel.
	if (this != nullptr) {
		if (Value < 0.0f) {
			//Controller->SetControlRotation(FRotator(0.0, 180.0f, 0.0f));
			hvr_rotateLeft();
		} else if (Value > 0.0f) {
			//Controller->SetControlRotation(FRotator(0.0, 0.0f, 0.0f));
			hvr_rotateRight();
		}
	}

	// Apply the input to the character motion
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Value);
}

void AhalfvrCharacter::lookRight(float value) {
	moveCameraAlongX += value;
}

void AhalfvrCharacter::lookUp(float value) {
	moveCameraAlongZ += value;
}

void AhalfvrCharacter::TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location) {
	// jump on any touch
	Jump();
}

void AhalfvrCharacter::TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location) {
	StopJumping();
}

///////////////////////////////////////////////////////
// HDM view
FRotator AhalfvrCharacter::GetViewRotation() const {
	// Get the view rotation from playerController class
	if (AHalfvrPlayerController* playerController = Cast<AHalfvrPlayerController>(Controller)) {
		return playerController->GetViewRotation();
	}

	else if (Role < ROLE_Authority) {
		// check if being spectated
		for (FConstPlayerControllerIterator Iterator = GetWorld()->GetPlayerControllerIterator(); Iterator; ++Iterator) {
			APlayerController* PlayerController = *Iterator;
			if (PlayerController && PlayerController->PlayerCameraManager->GetViewTargetPawn() == this) {
				return PlayerController->BlendedTargetViewRotation;
			}
		}
	}
	
	return GetActorRotation();
}

//////////////////////////////////////////////////////////////////////////////////////////
// Private fucntions

// Rotate the character to face right
void AhalfvrCharacter::hvr_rotateRight() {
	facingRight = true;
	this->SetActorRelativeRotation(FRotator(0.0, 0.0f, 0.0f));
}

// rotate the character to face left
void AhalfvrCharacter::hvr_rotateLeft() {
	facingRight = false;
	this->SetActorRelativeRotation(FRotator(0.0, 180.0f, 0.0f));
}

// Get location of character
const FVector AhalfvrCharacter::getLocation() {
	return this->GetActorLocation();
}

void AhalfvrCharacter::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
	
	elapsedTime += DeltaTime;
	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Elapsed Time: %f"), elapsedTime));

	
	//ReceiveActorBeginOverlap(flag);

	FRotator tempOR = oculusRotation;
	oculusRotation = GetViewRotation();// .Clamp(); <-- Use if you want range [0, 360] instead of the current range [-180, 180]
	
	trans->SetRotation(FQuat(FRotator(/*oculusRotation.Pitch*/0.0f, -90.0f, 0.0f)));

	const FVector PlayerVelocity = GetVelocity();
	float playerSpeed = PlayerVelocity.X;

	if (oculusRotation.Yaw > 100 && tempOR.Yaw < 0) {
		oculusRotation.Yaw = -maxRotation;
	} else if (oculusRotation.Yaw < 0 && tempOR.Yaw > 100) {
		oculusRotation.Yaw = maxRotation;
	}

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Oculus Pitch: %f"), oculusRotation.Pitch));
	
	if (!facingRight) {
		// For use with Oculus Rift rotation
		cameraTranslation = FVector(-oculusRotation.Yaw * ORMultiplier, 0, oculusRotation.Pitch * ORMultiplier);
		parallaxEffect->scrollRight(abs(playerSpeed), -oculusRotation.Yaw);

		// For use with controller bumpers
		//cameraTranslation = FVector(-moveCameraAlongX, 0, moveCameraAlongZ);
		//parallaxEffect->scrollRight(abs(playerSpeed), -moveCameraAlongX);
	} else if (facingRight) {
		// For use with Oculus Rift rotation
		cameraTranslation = FVector(oculusRotation.Yaw * ORMultiplier, 0, oculusRotation.Pitch * ORMultiplier);
		parallaxEffect->scrollLeft(abs(playerSpeed), -oculusRotation.Yaw);

		// For use with controller bumpers
		//cameraTranslation = FVector(moveCameraAlongX, 0, moveCameraAlongZ);
		//parallaxEffect->scrollLeft(abs(playerSpeed), -moveCameraAlongX);
	}

	//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("Oculus Yaw: %f"), cameraTranslation.X));
	trans->SetTranslation(FVector(cameraTranslation));
	
	boom->SetRelativeTransform(*trans);
}

void AhalfvrCharacter::resetLevel() {
	deaths++;
	SetActorLocation(FVector(0.0f, -200.0f, 0.0f));
	parallaxEffect->resetLevel();
}

void AhalfvrCharacter::print() {
	std::ofstream logFile;
	logFile.open("D:\\Bachelor\\halfvr\\Binaries\\timeLog.txt", std::ios_base::app);
	logFile << "Using the Oculus Rift Tracker:\n";
	//logFile << "Using the controller bumpers:\n";
	logFile << "Time spent in seconds:\t" << elapsedTime << "\n";
	logFile << "Number of times died:\t" << deaths << "\n\n";
	logFile.close();
}