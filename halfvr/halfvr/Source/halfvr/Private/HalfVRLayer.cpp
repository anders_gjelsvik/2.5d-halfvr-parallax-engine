// Fill out your copyright notice in the Description page of Project Settings.

#include "halfvrPrivate.h"
#include "HalfVRLayer.h"
#include "halfvrCharacter.h"

AHalfVRLayer::AHalfVRLayer(int l, int d, float s, float orm, std::string t) {
	layer = l;
	depth = d;
	type = t;

	scrolled = 0;
	oculusRotationMultiplier = orm;

	FVector currentCPPosition;
	for (TObjectIterator<AhalfvrCharacter> Itr; Itr; ++Itr) {
		currentCPPosition = Itr->GetActorLocation();
	}

	if (type.compare("play") == 0) {
		speedMultiplier = 0;
	} else {
		speedMultiplier = s;
		for (TObjectIterator<APaperSpriteActor> Itr; Itr; ++Itr) {
			FVector pos = Itr->GetActorLocation();
			if ((int)pos.Y == depth) {
				objects.push_back(Itr);
				objectStartPosition.push_back(pos);
				pos.X = currentCPPosition.X;
				//Itr->SetActorLocation(pos);
				pActor = Itr;
			}
		}
	} 

	UE_LOG(LogTemp, Warning, TEXT("Layer: %i, Depth: %i, SpeedMultiplier: %f"), layer, depth, speedMultiplier);
}

void AHalfVRLayer::scroll(float playerSpeed, float oculusRotation, bool scrollRight) {
	float playerSpeedDivider = 100;
	
	FVector cameraPosition;

	for (TObjectIterator<AhalfvrCharacter> Itr; Itr; ++Itr) {
		cameraPosition = Itr->GetCameraBoom()->GetComponentLocation();
	}

	float speed = (playerSpeed / playerSpeedDivider) * speedMultiplier;
	scrolled = scrollRight ? (scrolled + speed) : (scrolled - speed);
	oculusRotation *= (oculusRotationMultiplier * speedMultiplier);

	int i = 0;
	for (std::vector<TObjectIterator<APaperSpriteActor>>::iterator it = objects.begin(); it != objects.end(); it++) {
		FVector object = (*it)->GetActorLocation();
		object.X = objectStartPosition[i].X + cameraPosition.X + scrolled + oculusRotation;
		if (speedMultiplier != 0) {
			(*it)->SetActorLocation(object);
		}
		i++;
	}
}

void AHalfVRLayer::resetLevel() {
	int i = 0;
	scrolled = 0;
	for (std::vector<TObjectIterator<APaperSpriteActor>>::iterator it = objects.begin(); it != objects.end(); it++) {
		(*it)->SetActorLocation(objectStartPosition[i]);
		i++;
	}
}