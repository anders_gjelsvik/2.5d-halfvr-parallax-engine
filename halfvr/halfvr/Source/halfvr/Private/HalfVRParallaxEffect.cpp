// Fill out your copyright notice in the Description page of Project Settings.

#include "halfvrPrivate.h"
#include "halfvrCharacter.h"
#include "HalfVRParallaxEffect.h"
#include <string>


HalfVRParallaxEffect::HalfVRParallaxEffect(	int numberOfFrontLayers, 
											int numberOfPlayLayers, 
											int numberOfBackLayers, 
											int frontLayerYPosition, 
											int spaceBetweenLayers,
											float oculusRotationMultiplier,
											float initialSpeedMultiplier, 
											float speedMultiplierDecrement, 
											bool countPlayLayers) {

	for (int i = 0; i < (numberOfFrontLayers); i++) {
		types.push_back("front");
	}
	for (int i = 0; i < (numberOfPlayLayers); i++) {
		types.push_back("play");
	}
	for (int i = 0; i < (numberOfBackLayers); i++) {
		types.push_back("back");
	}

	float speedMultiplier = initialSpeedMultiplier;
	for (int i = 0; i < types.size(); i++) {
		layers.push_back(new AHalfVRLayer(i, frontLayerYPosition + (i * spaceBetweenLayers), speedMultiplier, oculusRotationMultiplier, types[i]));
		
		if (countPlayLayers) {
			speedMultiplier -= speedMultiplierDecrement;
		} else {
			if (types[i].compare("play") != 0) {
				speedMultiplier -= speedMultiplierDecrement;
			}
		}
	}
}

HalfVRParallaxEffect::~HalfVRParallaxEffect() {
	
}

void HalfVRParallaxEffect::scrollLeft(float playerSpeed, float oculusRotation) {
	for (std::vector<AHalfVRLayer*>::iterator it = layers.begin(); it != layers.end(); it++) {
		(*it)->scroll(playerSpeed, oculusRotation, false);
	}
}

void HalfVRParallaxEffect::scrollRight(float playerSpeed, float oculusRotation) {
	for (std::vector<AHalfVRLayer*>::iterator it = layers.begin(); it != layers.end(); it++) {
		(*it)->scroll(playerSpeed, oculusRotation, true);
	}
}

void HalfVRParallaxEffect::resetLevel() {
	for (std::vector<AHalfVRLayer*>::iterator it = layers.begin(); it != layers.end(); it++) {
		(*it)->resetLevel();
	}
}