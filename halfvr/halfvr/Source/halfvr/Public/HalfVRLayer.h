// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "PaperSpriteActor.h"
#include <string>
#include <vector>

/**
 * 
 */

class AHalfVRLayer
{

private:
	int depth;
	int layer;

	float scrolled;
	float speedMultiplier;
	float oculusRotationMultiplier;

	std::string type;
	TObjectIterator<APaperSpriteActor> pActor;
	std::vector<TObjectIterator<APaperSpriteActor>> objects;
	std::vector<FVector> objectStartPosition;

public:
	AHalfVRLayer(int l, int d, float s, float orm, std::string t);
	void scroll(float playerSpeed, float oculusRotation, bool scrollRight);

	void resetLevel();
};
