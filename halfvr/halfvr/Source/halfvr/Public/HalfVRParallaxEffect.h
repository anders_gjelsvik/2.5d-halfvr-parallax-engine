// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include <HalfVRLayer.h>
#include <vector>
#include "PaperSpriteActor.h"

/**
 * 
 */
class HALFVR_API HalfVRParallaxEffect
{

private:
	std::vector<AHalfVRLayer*> layers;
	std::vector<std::string> types;

public:
	HalfVRParallaxEffect(	int numberOfFrontLayers,			// Number of front layers
							int numberOfPlayLayers,				// Number of playable layers (these are ignored by the parallax effect)
							int numberOfBackLayers,				// Number of background layers
							int frontLayerYPosition,			// The Y psition of the very first front layer
							int spaceBetweenLayers,				// The space to be put between each layer
							float oculusRotationMultiplier,		// The number to be multiplied with the Oculus Rift rotation. Set to 0 if you don't use Oculus Rift
							float initialSpeedMultiplier,		// The multiplier used on the very first front layer
							float speedMultiplierDecrement,		// The number with which the speed multiplier will decrease for each layer
							bool countPlayLayers);				// Boolean to decide whether or not to include the playable layers when decreasing the speed multiplier

	~HalfVRParallaxEffect();

	void scrollLeft(float playerSpeed, float oculusRotation);
	void scrollRight(float playerSpeed, float oculusRotation);
	
	void resetLevel();
};
