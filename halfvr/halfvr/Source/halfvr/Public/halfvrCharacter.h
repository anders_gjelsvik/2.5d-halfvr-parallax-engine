	// Copyright 1998-2014 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "PaperCharacter.h"
#include "HalfVRParallaxEffect.h"
#include "halfvrCharacter.generated.h"

// This class is the default character for halfvr, and it is responsible for all
// physical interaction between the player and the world.
//
//   The capsule component (inherited from ACharacter) handles collision with the world
//   The CharacterMovementComponent (inherited from ACharacter) handles movement of the collision capsule
//   The Sprite component (inherited from APaperCharacter) handles the visuals

UCLASS(config=Game)
class AhalfvrCharacter : public APaperCharacter
{
	GENERATED_BODY()

	/** Side view camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera, meta=(AllowPrivateAccess="true"))
	class UCameraComponent* SideViewCameraComponent;

	/** Camera boom positioning the camera beside the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

private:
	bool facingRight;
	float maxRotation, ORMultiplier;
	float value;
	float moveCameraAlongX, moveCameraAlongZ;
	float elapsedTime;
	int deaths;

	HalfVRParallaxEffect* parallaxEffect;
	USpringArmComponent *boom;
	FVector cameraTranslation;
	FTransform *trans;
	FRotator oculusRotation;

	void hvr_rotateRight();
	void hvr_rotateLeft();

protected:
	// The animation to play while running around
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Animations)
	class UPaperFlipbook* RunningAnimation;

	// The animation to play while idle (standing still)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* IdleAnimation;

	/** Called to choose the correct animation to play based on the character's movement state */
	void UpdateAnimation(float value);

	/** Called for side to side input */
	void MoveRight(float Value);

	void lookRight(float value);

	void lookUp(float value);

	/** Handle touch inputs. */
	void TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location);

	/** Handle touch stop event. */
	void TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location);

	// APawn interfaces
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	virtual void BeginPlay() override;
	// End of APawn interfaces

	void timerUpdate();

public:
	AhalfvrCharacter(const FObjectInitializer& ObjectInitializer);

	/** Returns SideViewCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	/**	Returns the view rotation from the player controller **/
	virtual FRotator GetViewRotation() const override;

	const FVector getLocation();

	void Tick(float DeltaTime);

	FVector getBoomLocation();

	UFUNCTION(BlueprintCallable, Category = "print") void resetLevel();

	UFUNCTION(BlueprintCallable, Category = "print") void print();
};

//UFUNCTION(BlueprintCallable, Category = "print") 