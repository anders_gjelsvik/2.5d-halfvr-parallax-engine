// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "HalfvrPlayerController.generated.h"

/**
 *	Class inherit PlayerController
 *	The class set and controls the virtual reality view
 *	There are some minor issues at the moment when it comes to player 
 *	and the camera
 */
UCLASS()
class HALFVR_API AHalfvrPlayerController : public APlayerController
{
	GENERATED_BODY()
public:

	// Update the rotation of the camera
	virtual void UpdateRotation(float DeltaTime) override;

	// Get a pawns viewRotation from a default blueprin(pawn)
	UFUNCTION(BlueprintCallable, Category = "Pawn")
	FRotator GetViewRotation() const;

	// Get a pawns setViewRotation from a default blueprint(pawn)
	UFUNCTION(BlueprintCallable, Category = "Pawn")
	virtual void SetViewRotation(const FRotator& NewRotation);

	// Set the new control rotation
	virtual void SetControlRotation(const FRotator& NewRotation) override;

protected:
	/**
	*  View & Movement direction are now separate.
	*  The controller rotation will determine which direction we will move.
	*  ViewRotation represents where we are looking.
	*/
	UPROPERTY()
	FRotator ViewRotation;
};